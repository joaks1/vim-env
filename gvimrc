" File ~/.gvimrc

" Set window size
" set lines=9999
" set columns=140

" Set default color scheme
" colorscheme nefertiti
colorscheme lucius
LuciusDark

" After updating to Ubuntu 17.04, underscores no longer show up in gvim unless
" the linespacing is increased. Vanilla vim is not affected
set linespace=2
